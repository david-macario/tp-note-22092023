dir_to_save="$HOME"
backup_dir="$HOME/BACKUP"  

if [ ! -d "$backup_dir" ]; then
    mkdir -p "$backup_dir"
fi

backup_subdir="$backup_dir/$(date)"
mkdir -p "$backup_subdir"

cp -r "$dir_to_save"/* "$backup_subdir"

# Crontab qui exécute le script tout les jours à Minuit
# 0 0 * * * bash /Users/david/Documents/ENSITECH/NABIL/gestion-projet/tp/exo9.sh >/dev/null 2>&1
